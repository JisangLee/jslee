package com.kakaologin.kloginsample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.kakao.auth.ErrorCode;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.helper.log.Logger;

import org.w3c.dom.Text;

/**
 * Created by 지상 on 2016-08-03.
 */
public class KakaoSignupActivity extends Activity{
    public TextView kakaoId;
    public TextView kakaoNick;
    /**
     * Main으로 넘길지 가입 페이지를 그릴지 판단하기 위해 me를 호출한다.
     * @param savedInstanceState 기존 session 정보가 저장된 객체
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestMe();
    }
    /**
     * 사용자의 상태를 알아 보기 위해 me API 호출을 한다.
     */
    protected void requestMe(){ //유저의 정보를 받아오는 메소드
        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                String message = "Failed to get user info. msg="+errorResult;
                Logger.d(message);

                ErrorCode result = ErrorCode.valueOf(errorResult.getErrorCode());
                if(result == ErrorCode.CLIENT_ERROR_CODE){
                    finish();
                }else{
                    redirectLoginActivity();
                }
                //super.onFailure(errorResult);
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                redirectLoginActivity();
            }

            @Override
            public void onNotSignedUp() {
                //카카오톡 회원이 아닐 시 showSignup();으로 호출해야함.
            }

            @Override
            public void onSuccess(UserProfile userProfile) {//성공 시 userProfile형태로 변환
                setContentView(R.layout.activity_login);
                String kakaoID = String.valueOf((userProfile.getId()));//userProfile에서 ID값을 가져옴
                String kakaoNickname = userProfile.getNickname(); //Nickname값을 가져옴.
                kakaoId = (TextView) findViewById(R.id.kakaoId);
                kakaoNick = (TextView)findViewById(R.id.kakaoNick);
                kakaoId.setText(kakaoID);
                kakaoNick.setText(kakaoNickname);

                Log.d("id",kakaoID);
                Log.d("nick",kakaoNickname);
                Logger.d("UserProfile : "+ userProfile); //유저정보값
//                redirectMainActivity(); //redirectActivity메소드가 밑에보면 startActivity라 새롭게 시작하는거임...그래서 위에 카카오아이디랑 닉넴안뜸...새로이 시작해부리니까..
            }
        });
    }
    private void redirectMainActivity(){
        startActivity(new Intent(this,LoginActivity.class));//현재는 로그엔 액티비티가 메인이라..요거로햇슴.
        finish();
    }
    protected void redirectLoginActivity(){
        final Intent intent = new Intent(this,LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }
}
